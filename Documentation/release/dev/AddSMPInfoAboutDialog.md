# Add SMP backend information to about dialog

Adds the following in the Help->About dialog:
- SMP Tools backend in use
- SMP max number of threads
