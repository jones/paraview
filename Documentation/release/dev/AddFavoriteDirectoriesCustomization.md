## Add favorite directories customization

* Add "Add current directory to favorites", "Remove current directory from favorites" and "Reset favorites to system default" buttons for the favorites list in the file dialog.

* Add an "Add to favorites" option when right-clicking a directory in the files list.

* Add a "Remove from favorites" option when right-clicking a directory in the favorites list.

* Add a "Rename" option when right-clicking and item in the files list.

* Add a "Open in file explorer" option when right-clicking the files list. If the right-clicked item is a directory, it is opened in the system file explorer. If a file or nothing is select, it is the current directory that is opened.

* Add a "Delete empty directory" when right-clicking an empty directory in the files list.

* Add a "Rename label" option when right-clicking a directory in the favorites list, which can also be triggered by pressing the F2 key. It only renames the label displayed in the favorites list and not the actual folder name.

* Add standard shortcuts to some buttons in the file dialog. `Alt+Left` goes back, `Alt+Right` goes forward, `Alt+Up` goes to the parent directory and `Ctrl+N` creates a new directory.

* Add labels for favorites and recent lists to show the user what they are.

* Disable the "Create New Folder" button when opening an existing file.

* Add feedback when a favorites path does not exist : the name is greyed out and in italic, the icon is replaced by a warning and the tooltip warns that the path does not exist.
